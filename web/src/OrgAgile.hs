-- |

module OrgAgile where

import OrgAgile.App
import OrgAgile.Config
import OrgAgile.Core.Auth
import OrgAgile.Core.Log
import OrgAgile.Api
import Network.Wai (Middleware, Request, requestHeaders)
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.RequestLogger.JSON
import RIO
import Servant hiding (runHandler)
import Servant.Server.Experimental.Auth




type AuthContext = AuthHandler Request User ': AuthHandler Request (Maybe User) ': '[]

mkApp :: Env -> Application
mkApp env =
    serveWithContext api serverAuthContext hoistedServer
  where
    serverAuthContext = handleAuthentication env :. handleOptionalAuthentication env :. EmptyContext

    hoistedServer = hoistServerWithContext api (Proxy :: Proxy AuthContext) (runHandler env) server

startApp :: Config -> IO ()
startApp cfg = do
    pool <- loadPool (configConnectionString cfg) (configPoolSize cfg)
    result <- autoMigrate pool
    whenJust result $ \e -> do
        error $ show e
    let env = Env cfg pool
    runApp (configPort cfg) env

runApp :: Int -> Env -> IO ()
runApp port env = do
    warpLogger <- jsonRequestLogger
    let warpSettings = setPort port defaultSettings
    runSettings warpSettings $ warpLogger $ mkApp env

