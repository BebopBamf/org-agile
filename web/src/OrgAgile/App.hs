{- |
Module      :  App.hs
Description :  Application primitives
License     :  AGPL-3.0
Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
Stability   :  experimental
Portability :  POSIX

This module contains the base application primitives.
-}
module OrgAgile.App (AppM, Env (..), runHandler) where

import Control.Monad.Trans.Except (ExceptT (..))
import Hasql.Pool (Pool)
import RIO
import qualified Servant

data Env = Env
    {envPool :: !Pool}

type AppM = RIO Env

runHandler :: Env -> AppM a -> Servant.Handler a
runHandler env app = Servant.Handler $ ExceptT $ try $ runRIO env app
