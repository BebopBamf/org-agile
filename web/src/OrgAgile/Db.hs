{- |
Module      :  Init.hs
Description :  Initialize the application with some default settings
License     :  AGPL-3.0
Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
Stability   :  experimental
Portability :  POSIX

This module initializes the application with some default settings.
-}
module Db where
