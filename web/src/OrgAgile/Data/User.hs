{- |
Module      :  Models.User.hs
Description :  User model
License     :  AGPL-3.0
Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
Stability   :  experimental
Portability :  POSIX

This module defines the user model.
-}
module Models.User where

import RIO
import Data.UUID (UUID)

data User = User
    { userId :: !UUID
    , userName :: Text
    , userEmail :: Text
    }
    deriving (Show)
