{- |
Module      :  Models.hs
Description :  Models rexports
License     :  AGPL-3.0
Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
Stability   :  experimental
Portability :  POSIX

This module initializes the application with some default settings.
-}
module Models
( module Models.User )
where

import Models.User
