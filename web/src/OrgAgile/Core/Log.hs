-- |

module OrgAgile.Core.Log where

import RIO
import Network.Wai (Middleware)
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.RequestLogger.JSON

jsonRequestLogger :: IO Middleware
jsonRequestLogger =
    mkRequestLogger
        $ defaultRequestLoggerSettings
            { outputFormat = CustomOutputFormatWithDetails formatAsJSON
            }
