{- |
Module      :  Config.hs
Description :  Configuration primitives
License     :  AGPL-3.0
Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
Stability   :  experimental
Portability :  POSIX

This module contains the base configuration primitives.
-}
module Config where

import RIO

data Config = Config
    { configPort :: !Int
    , configConnectionString :: !ByteString
    , configPoolSize :: !Int
    }
    deriving (Show)
