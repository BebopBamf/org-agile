{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      :  Api.hs
Description :  API type definition
License     :  AGPL-3.0
Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
Stability   :  experimental
Portability :  POSIX

This module initializes the application with some default settings.
-}
module Api where

import App
import RIO
import Servant

type API = "api" :> "v1" :> "hello" :> Get '[JSON] Text

api :: Proxy API
api = Proxy

server :: ServerT API AppM
server = return "Hello, world!"
