module Main (main) where

import Init

main :: IO ()
main = do
    config <- loadConfig
    startApp config
